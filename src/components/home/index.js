import React from 'react';
import Header from './../layout/header';
import Stocks from './../stocks/index';
import TradingStockChart from './../charts/index';
import {
    Container,
    Row,
} from 'react-bootstrap';


const Dashboard= (props) =>{
    return(
        <div>
            <Header/>
            <Container className="container my-5">
                <h2 className="text-center text-primary my-5">MARKET ACTION</h2>
                <Row>
                    <div className="col-md-6 border p-0 bg-light">
                        <Stocks/>
                    </div>
                    <div className="col-md-6">
                        <TradingStockChart isStockFilter={true}/>
                    </div>
                </Row>
            </Container>
        </div>
    );
}

export default Dashboard;