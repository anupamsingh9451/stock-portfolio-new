import React from 'react';
import Header from './../layout/header';
import UserPortfolio from './portfolio';
import TradingStockChart from './../charts/index';
import {
    Container,
    Row,
} from 'react-bootstrap';


const Portfolio = (props) =>{
    return(
        <div>
            <Header/>
            <Container className="container my-5">
                <h2 className="text-center text-primary my-5">PORTFOLIO</h2>
                <Row>
                    <div className="col-md-6 border p-0 bg-light">
                        <UserPortfolio/>
                    </div>
                    <div className="col-md-6">
                        <TradingStockChart isStockFilter={true}/>
                    </div>
                </Row>
            </Container>
        </div>
    );
}

export default Portfolio;