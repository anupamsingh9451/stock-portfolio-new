import React,{useState,useEffect} from 'react';
import Header from '../layout/header';
import StockDetails from '../stocks/stockDetails';
import {useParams } from 'react-router-dom';
import TradingStockChart from '../charts/index';
import {stocksData} from '../../services/stocksData';
import SellForm from './sellForm';
import stockTransaction from '../../services/stockTransaction';
import {
    Container,
    Row,
} from 'react-bootstrap';


const Buy= () =>{
    const props = useParams();
    let stock = stocksData.find(stock=>stock.id?.toString()===props.id?.toString());
    const [stockInHold,setStockInHold]=useState(0);

    useEffect(()=>{
        getStockInHand()
    },[])

    const getStockInHand= () =>{
        stockTransaction
        .getStocks(props.id)
        .then((response) => {
            if(response.data.code===200){
                if(response.data?.data?.quantity){
                    setStockInHold(response.data?.data?.quantity)
                }
            }
        })   
    }
    
    return(
        <div>
            <Header/>
            <Container className="container my-5">
                <h2 className="text-center text-primary my-5">Sell - {stock?.name}</h2>
                <Row>
                    <div className="col-md-6 border p-0">
                        <StockDetails stock={stock} stockInHold={stockInHold} setStockInHold={setStockInHold} getStockInHand={getStockInHand}/>
                        <SellForm stock={stock} stockInHold={stockInHold} setStockInHold={setStockInHold} getStockInHand={getStockInHand}/>
                    </div>
                    <div className="col-md-6">
                        <TradingStockChart isStockFilter={false}/>
                    </div>
                </Row>
            </Container>
        </div>
    );
}

export default Buy;