import React,{useEffect} from 'react';
import {stocksData} from '../../services/stocksData';
import {
    OverlayTrigger,
    Tooltip
} from 'react-bootstrap';

export const StockDataSelect = (props) =>{
    let className = props?.className;
    let value = props?.value ? props?.value: ((stocksData?.length>0)?stocksData[0]?.id:'');
    let placeholder = props?.placeholder ? props?.placeholder :'';
    let toolTip =props?.isToolTip ?<Tooltip> {props?.placeholder}</Tooltip>:'';

    const getText = (e) =>{
        return e.options[e.selectedIndex].label;
    }
    return (
        <OverlayTrigger overlay={toolTip}>
            <select 
                className={"form-control "+className}
                placeholder={placeholder}
                title={placeholder}
                value={value}
                onChange={
                    (event)=>{
                        props.onChange(props?.name,event.target.value,getText(event.target))
                    }
                }
            >
                {
                    stocksData?.map((stock,index)=>{
                        return (
                            <option value={stock?.id} key={index}>
                                {stock?.name}
                            </option>
                        );
                    })
                }
            </select>
        </OverlayTrigger>
    );
}