import React from 'react';
import {
    Table,
} from 'react-bootstrap';
import {stocksData} from '../../services/stocksData';
import StockDataTd from './stockData';

const Stocks= (props) =>{
    return(
        <div>
            <Table striped  hover size="sm" variant="light" className="m-0">
                <thead>
                    <tr>
                        <th>Index</th>
                        <th>Price</th>
                        <th>Change</th>
                        <th>%Chg</th>
                        <th>Buy</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        stocksData?.map((stock,index)=>{
                            return (
                                <StockDataTd stock={stock} key={index}/>
                            );
                        })
                    }
                </tbody>
            </Table>
        </div>
    );
}

export default Stocks;