export const API_BASE_URL = process.env.REACT_APP_SERVER_URL;
export const ACCESS_TOKEN_NAME = 'login_access_token';
export const PROJECT_NAME ="Stock Portfolio";